declare module 'slash2';
declare module '*.css';
declare module '*.less';
declare module '*.scss';
declare module '*.sass';
declare module '*.svg';
declare module '*.png';
declare module '*.jpg';
declare module '*.jpeg';
declare module '*.gif';
declare module '*.bmp';
declare module '*.tiff';
declare module 'omit.js';

// google analytics interface
interface GAFieldsObject {
  eventCategory: string;
  eventAction: string;
  eventLabel?: string;
  eventValue?: number;
  nonInteraction?: boolean;
}
interface Window {
  ga: (
    command: 'send',
    hitType: 'event' | 'pageview',
    fieldsObject: GAFieldsObject | string,
  ) => void;
  reloadAuthorized: () => void;
}

declare let ga: Function;

// preview.pro.ant.design only do not use in your production ;
// preview.pro.ant.design 专用环境变量，请不要在你的项目中使用它。
declare let ANT_DESIGN_PRO_ONLY_DO_NOT_USE_IN_YOUR_PRODUCTION: 'site' | undefined;

declare const REACT_APP_ENV: 'test' | 'dev' | 'pre' | false;
//
//# 开发环境配置
declare let ENV = 'development'
//# port = '8888'
//# 易立德OA / 开发环境
declare let BASE_API = '/dev-api'
//# 路由懒加载
declare let VUE_CLI_BABEL_TRANSPILE_MODULES = true
//# 前端地址
declare let PORTAL_HOST_URL = 'http://localhost/'
//# 易立德OA
declare let OA_HOST_URL = 'http://testoa.e-lead.cn/oa/'
//# 上传地址
declare let UPLOAD_URL = 'http://testoa.e-lead.cn/oa/'
//# 单点服务后台
declare let SSO_HOST_URL = 'http://sso.e-lead.cn/sso/'
//# SSO
//# corpId
declare let SSO_CORP_ID = 'ding72d45292a8b0a17024f2f5cc6abecb85'
//# 浏览器存储的键
declare let SSO_SESSION_TOKEN_KEY = 'sso-token'
declare let SSO_SESSION_USER_KEY = 'sso-user'
//# 认证TOEKN
declare let SSO_HEADER_TOKEN = 'Authorization'
//# 认证方式
declare let SSO_RESPONSE_TYPE = 'code'
//# 认证客户端ID
declare let SSO_CLIENT_ID = 'elead_oa'
//# 单点退出地址
//#SSO_LOGOUT_URL = 'code'
//# 登录成功回调地址
declare let SSO_CALLBACK_URL = 'codeCallback'

