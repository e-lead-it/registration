import moment from "moment";
const defaultSet = {
  get PAGINATION() {
    return {
      current: 1,//pageNum
      pageSize: 7,
      total: 1,
      size: 'small',
      pageSizeOptions: ['5', '10', '15', '20', '25', '30'],
      showSizeChanger: true,
      showQuickJumper: true,
      showTotal: (total, range) => `从第 ${range[0]} 条 ~ 第${range[1]} 条, 总共 ${total} 条记录`
    };
  },
  get DAYRANGE() {
    return {
      // eventDayStart: moment().subtract(1, 'weeks').startOf('week').format(defaultSet.dateFormat),
      // eventDayEnd: moment().subtract(1, 'weeks').endOf('week').format((defaultSet.dateFormat)),
      eventDayStart: moment().subtract(7, 'd').format((defaultSet.dateFormat)),
      eventDayEnd: moment().subtract(1, 'd').format(defaultSet.dateFormat),
      //eventDayStart: moment("2020-2-1").format((defaultSet.dateFormat)),
      //eventDayEnd: moment().format(defaultSet.dateFormat),
    }
  },
  get DATE_FORMAT() {
    return "YYYY-MM-DD"
  },
  get TIME_FORMAT() {
    return "h:mm:ss a"
  },
  get DEFAULT_OPTIONS() {
    return [{key: "", label: ""}]
  },
  get DEFAULT_CURRENT_SELECT() {
    return {key: "", label: ""}
  },
}
export default defaultSet;

