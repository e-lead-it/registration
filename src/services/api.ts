import request from '@/utils/request';
import {Urls} from "@/services/urls";
import {getParams} from "@/utils/utils";
//拿code
export async function codeCallback(params?: any): Promise<any> {
  return request(OA_HOST_URL + Urls.GET_TOKEN + getParams(params), {
    method: 'GET'
  })
}
//拿用户信息
export async function getUserInfo(params?: any): Promise<any> {
  return request(OA_HOST_URL + Urls.GET_USER_INFO + getParams(params), {
    method: 'GET'
  })
}
//退出登录
export async function logout(params?: any): Promise<any> {
  return request(OA_HOST_URL + Urls.LOGOUT + getParams(params), {
    method: 'POST'
  })
}
//取菜单
export async function getRouters(params?: any): Promise<any> {
  return request(OA_HOST_URL + Urls.GET_ROUTERS + getParams(params), {
    method: 'GET'
  })
}
//部门成员
export async function getEmployee(params?: any): Promise<any> {
  return request(OA_HOST_URL + Urls.EMP_EMPLOYEE_PAGE + getParams(params), {
    method: 'GET'
  })
}
//部门信息
export async function orgDept(params?: any): Promise<any> {
  return request(OA_HOST_URL + Urls.ORG_DEPT + '/' + params, {
    method: 'GET'
  })
}
//编辑部门信息
export async function saveOrgDept(params?: any): Promise<any> {
  return request(OA_HOST_URL + Urls.ORG_DEPT, {
    method: 'PUT',
    data: params
  })
}
//获取上级部门
export async function orgDeptAncestors(params?: any): Promise<any> {
  return request(OA_HOST_URL + Urls.ORG_DEPT_ANCESTORS + '/' + params, {
    method: 'GET'
  })
}
//获取下级部门
export async function orgDeptChildren(params?: any): Promise<any> {
  return request(OA_HOST_URL + Urls.ORG_DEPT_CHILDREN + '/' + params, {
    method: 'GET'
  })
}
export async function listDept(params?: any): Promise<any> {
  return request(OA_HOST_URL + Urls.LIST_DEPT + getParams(params), {
    method: 'GET'
  })
}
//部门级别
export async function orgDeptGrade(params?: any): Promise<any> {
  return request(OA_HOST_URL + Urls.ORG_DEPT_GRADE + getParams(params), {
    method: 'GET'
  })
}
//学历
export async function empEduEnrollment(): Promise<any> {
  return request(OA_HOST_URL + Urls.EMP_EDU_ENROLLMENT, {
    method: 'GET'
  })
}
export async function empEduDegree(): Promise<any> {
  return request(OA_HOST_URL + Urls.EMP_EDU_DEGREE, {
    method: 'GET'
  })
}
export async function empEduEducation(): Promise<any> {
  return request(OA_HOST_URL + Urls.EMP_EDU_EDUCATION, {
    method: 'GET'
  })
}
