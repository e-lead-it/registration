export enum Urls {
  //取code
  GET_TOKEN = "codeCallback",
  //个人信息
  GET_USER_INFO = "getInfo",
  //登出
  LOGOUT = "logout",
  //取菜单那
  GET_ROUTERS = "getRouters",
  //
  LIST_DEPT = "org/dept/list",
  ORG_DEPT = "org/dept",
  //获取上级部门
  ORG_DEPT_ANCESTORS = "org/dept/ancestors",
  //获取下级部门
  ORG_DEPT_CHILDREN = "org/dept/children",
  EMP_EMPLOYEE_PAGE = "emp/employee/page",
  //根据字典类型查询字典数据信息
  ORG_DEPT_GRADE = "system/dict/data/dictType/org_dept_grade",
  //学历
  EMP_EDU_EDUCATION = "system/dict/data/dictType/emp_edu_education",
  EMP_EDU_DEGREE = "system/dict/data/dictType/emp_edu_degree",
  EMP_EDU_ENROLLMENT = "system/dict/data/dictType/emp_edu_enrollment",


}
