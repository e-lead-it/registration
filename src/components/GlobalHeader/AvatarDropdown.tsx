import {LogoutOutlined} from '@ant-design/icons';
import {Avatar, Menu, Spin} from 'antd';
import {ClickParam} from 'antd/es/menu';
import React from 'react';
import {connect, ConnectProps, history} from 'umi';
import {ConnectState} from '@/models/connect';
import {CurrentUser} from '@/models/user';
import HeaderDropdown from '../HeaderDropdown';
import styles from './index.less';
import {get} from "lodash"
import {removeToken} from 'chaos-sso-login'
import {UserOutlined} from "@ant-design/icons/lib";
import {COLOR} from "@/services/color";
export interface GlobalHeaderRightProps extends Partial<ConnectProps> {
  currentUser?: CurrentUser;
  menu?: boolean;
}
class AvatarDropdown extends React.Component<GlobalHeaderRightProps> {
  onMenuClick = (event: ClickParam) => {
    const {key} = event;
    if (key === 'logout') {
      const {dispatch} = this.props;
      if (dispatch) {
        dispatch({
          type: 'login/logout',
        }).then((res: any) => {
          removeToken()
          window.location.reload()
        });
      }
      return;
    }
    history.push(`/account/${key}`);
  };
  render(): React.ReactNode {
    const {
      currentUser = {
        avatar: '',
        name: '',
      },
      menu,
    } = this.props;
    const menuHeaderDropdown = (
      <Menu className={styles.menu} selectedKeys={[]} onClick={this.onMenuClick}>
        {/*{menu && (
          <Menu.Item key="center">
            <UserOutlined />
            个人中心
          </Menu.Item>
        )}
        {menu && (
          <Menu.Item key="settings">
            <SettingOutlined />
            个人设置
          </Menu.Item>
        )}
        {menu && <Menu.Divider />}*/}
        <Menu.Item key="logout">
          <LogoutOutlined/>
          退出登录
        </Menu.Item>
      </Menu>
    );
    return currentUser && get(currentUser, "user", false) ? (
      <HeaderDropdown overlay={menuHeaderDropdown}>
        <span className={`${styles.action} ${styles.account}`}>
          {/*<Avatar size="small" className={styles.avatar} src={currentUser.avatar} alt="avatar" />*/}
          <Avatar style={{background: COLOR.FILLBLUE}} size="small" className={styles.avatar} alt="avatar"
                  icon={<UserOutlined/>}/>
          <span className={styles.name}>{get(currentUser, "user", "").userName}</span>
        </span>
      </HeaderDropdown>
    ) : (
      <span className={`${styles.action} ${styles.account}`}>
        <Spin
          size="small"
          style={{
            marginLeft: 8,
            marginRight: 8,
          }}
        />
      </span>
    );
  }
}
export default connect(({user}: ConnectState) => ({
  currentUser: user.currentUser,
}))(AvatarDropdown);
