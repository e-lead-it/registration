import {Effect, Reducer, Subscription} from 'umi';
import {NoticeIconData} from '@/components/NoticeIcon';
import {queryNotices} from '@/services/user';
import {ConnectState} from './connect.d';
import {getEmployee} from "@/services/api";
import {listDept} from "@/services/api";
import {orgDeptGrade} from "@/services/api";
export interface NoticeItem extends NoticeIconData {
  id: string;
  type: string;
  status: string;
}
export interface GlobalModelState {
  collapsed: boolean;
  notices: NoticeItem[];
  //成员
  employee: any,
  //部门列表
  listDept: any,
  //部门界别
  orgDeptGrade:any

}
export interface GlobalModelType {
  namespace: 'global';
  state: GlobalModelState;
  effects: {
    fetchNotices: Effect;
    clearNotices: Effect;
    changeNoticeReadState: Effect;
    fetchEmployee: Effect;
    fetchListDept: Effect;
    fetchOrgDeptGrade: Effect;
  };
  reducers: {
    changeLayoutCollapsed: Reducer<GlobalModelState>;
    saveNotices: Reducer<GlobalModelState>;
    saveClearedNotices: Reducer<GlobalModelState>;
    saveEmployee: Reducer<GlobalModelState>;
    saveListDept: Reducer<GlobalModelState>;
    saveOrgDeptGrade: Reducer<GlobalModelState>;
  };
  subscriptions: { setup: Subscription };
}
const GlobalModel: GlobalModelType = {
  namespace: 'global',
  state: {
    collapsed: false,
    notices: [],
    employee: null,
    listDept: [],
    orgDeptGrade:null
  },
  effects: {

    *fetchOrgDeptGrade({payload}, {call, put, select}) {
      const data = yield call(orgDeptGrade)
      let result = yield put({
        type: "saveOrgDeptGrade",
        payload: data
      })
      return Promise.resolve(result)
    },

    * fetchListDept({payload}, {call, put, select}) {
      const data = yield call(listDept)
      let result = yield put({
        type: "saveListDept",
        payload: data
      })
      return Promise.resolve(result)
    },
    * fetchEmployee({payload}, {call, put, select}) {
      const data = yield call(getEmployee, payload)
      let result = yield put({
        type: "saveEmployee",
        payload: data
      })



      return Promise.resolve(result)
    },
    * fetchNotices(_, {call, put, select}) {
      const data = yield call(queryNotices);
      yield put({
        type: 'saveNotices',
        payload: data,
      });
      const unreadCount: number = yield select(
        (state: ConnectState) => state.global.notices.filter((item) => !item.read).length,
      );
      yield put({
        type: 'user/changeNotifyCount',
        payload: {
          totalCount: data.length,
          unreadCount,
        },
      });
    },



    * clearNotices({payload}, {put, select}) {
      yield put({
        type: 'saveClearedNotices',
        payload,
      });
      const count: number = yield select((state: ConnectState) => state.global.notices.length);
      const unreadCount: number = yield select(
        (state: ConnectState) => state.global.notices.filter((item) => !item.read).length,
      );
      yield put({
        type: 'user/changeNotifyCount',
        payload: {
          totalCount: count,
          unreadCount,
        },
      });
    },
    * changeNoticeReadState({payload}, {put, select}) {
      const notices: NoticeItem[] = yield select((state: ConnectState) =>
        state.global.notices.map((item) => {
          const notice = {...item};
          if (notice.id === payload) {
            notice.read = true;
          }
          return notice;
        }),
      );
      yield put({
        type: 'saveNotices',
        payload: notices,
      });
      yield put({
        type: 'user/changeNotifyCount',
        payload: {
          totalCount: notices.length,
          unreadCount: notices.filter((item) => !item.read).length,
        },
      });
    },
  },
  reducers: {

    saveOrgDeptGrade(state: any, {payload}): GlobalModelState {
      return {
        ...state,
        orgDeptGrade: payload
      }
    },

    saveListDept(state: any, {payload}): GlobalModelState {
      return {
        ...state,
        listDept: payload
      }
    },
    saveEmployee(state: any, {payload}): GlobalModelState {
      return {
        ...state,
        employee: payload
      }
    },
    changeLayoutCollapsed(state: any, {payload}): GlobalModelState {
      return {
        ...state,
        collapsed: payload,
      };
    },
    saveNotices(state: any, {payload}): GlobalModelState {
      return {
        collapsed: false,
        ...state,
        notices: payload,
      };
    },
    saveClearedNotices(state: any, {payload}): GlobalModelState {
      return {
        ...state,
        collapsed: false,
        notices: state.notices.filter((item): boolean => item.type !== payload),
      };
    },
  },
  subscriptions: {
    setup({history}): void {
      // Subscribe history(url) change, trigger `load` action if pathname is `/`
      history.listen(({pathname, search}): void => {
        if (typeof window.ga !== 'undefined') {
          window.ga('send', 'pageview', pathname + search);
        }
      });
    },
  },
};
export default GlobalModel;
