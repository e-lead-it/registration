import {SelectOptionItem} from "@/pages/Public/Public.interface";

export interface OaUserSelect {
  currentValue: SelectOptionItem[],
  options: SelectOptionItem[],
  data?: any
}
