import React, {Component} from 'react';
import {connect} from 'dva';
import {ConnectState} from "@/models/connect";
import {get} from "lodash"
import {cloneDeep} from "lodash"
import {Select} from "antd";
import {SelectOptionItem} from "@/pages/Public/Public.interface";
import {OaUserSelect} from "@/pages/Public/OaUser/OaUser.interface";
import defaultSet from "@/services/defaultSet";
const {Option} = Select;
class OaUser extends Component {
  state = {
    userSelect: {},
    employee: {},
    selectType: {},
    searchCache: []
  };
  static defaultProps = {
    currentValue: "",
    mode: false,
    onChange: (res: any) => {
    }
  }
  getEmployee = () => {
    let employee = cloneDeep(get(this.props, "employee", {}))
    let dataSource: SelectOptionItem[] = employee.rows.map((res: any, index: number) => {
      res.key = res.id
      res.label = res.userName
      res.data = cloneDeep(res)
      return res
    })
    let currentValue = null
    //多选
    if (this.props.mode) {
      var arr = []
      if (this.props.currentValue.length > 0) {
        dataSource.forEach((item: any) => {
          this.props.currentValue.forEach((citem: any) => {
            if (item.label == citem) {
              arr.push(item)
            }
          })
        })
      }
      currentValue = arr
    }

    //单选
    else {
      var obj = defaultSet.DEFAULT_CURRENT_SELECT
      if (get(this.props, "currentValue", "") != null && get(this.props, "currentValue", "").trim() != "") {
        obj = dataSource.filter((item: any) => {
          return item.label == this.props.currentValue
        })[0]
      }
      currentValue = obj
    }
    let selectType: OaUserSelect = {
      currentValue,
      options: dataSource
    }
    this.setState({
      employee,
      selectType,
      searchCache: cloneDeep(dataSource)
    })
  }
  componentDidMount() {
    const {employee, dispatch} = this.props
    if (employee) {
      this.getEmployee()
    } else {
      if (dispatch) {
        dispatch({
          type: "global/fetchEmployee",
          payload: {
            pageNum: 1,
            pageSize: 100,
            status: 1
          }
        }).then((res: any) => {
          this.getEmployee()
        })
      }
    }
  }
  search = (str: any) => {
    let selectType: OaUserSelect = this.state.selectType as OaUserSelect
    let options: SelectOptionItem[] = cloneDeep(this.state.searchCache)
    if (str) {
      options = cloneDeep(this.state.searchCache).filter((res: SelectOptionItem) => {
        return res.label.indexOf(str) >= 0 || (get(res, "deptName", "")).indexOf(str) >= 0
      })
    } else {
      options = cloneDeep(this.state.searchCache)
    }
    selectType["options"] = options
    this.setState({
      selectType
    })
  }
  onChange = (selected: any) => {
    let selectType: OaUserSelect = this.state.selectType as OaUserSelect
    //多选
    if (this.props.mode) {
      var arr = []
      for (var i = 0; i < this.state.selectType.options.length; i++) {
        for (var a = 0; a < selected.length; a++) {
          if (this.state.selectType.options[i].key == selected[a].key) {
            arr.push(this.state.selectType.options[i].data)
          }
        }
      }
      selectType.currentValue = arr
      //多选会把一个数组发出去给父组件，数组里面是对象
      this.props.onChange(selectType.currentValue)
    }
    //单选
    else {
      selectType.currentValue = this.state.selectType.options.filter((item: any) => {
        return item.key == selected.key
      })[0]
      //单选直接把名字发出去，是一个字符串
      this.props.onChange(selectType.currentValue.label)
    }
    this.setState({
      selectType
    }, (res: any) => {
    })
  }
  render() {
    return <>
      <Select style={{width: "100%"}} mode={this.props.mode ? "multiple" : false}
              value={get(this.state.selectType, "currentValue", [{key: "", value: ""}])}
              labelInValue
              showSearch
              onChange={this.onChange} filterOption={false} onSearch={this.search}>
        {(get(this.state.selectType, "options", [])).map((res: any) => {
          return <Option value={res.key} key={res.key} title={res.label}>
            <div key={res.key} className="demo-option-label-item">
              {res.label} &nbsp; ( {res.data.deptName} )
            </div>
          </Option>
        })}
      </Select>
    </>
  }
}
export default connect(
  ({global}: ConnectState) => ({
    employee: global.employee
  }),
)(OaUser);
