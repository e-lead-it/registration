export interface SelectOptionItem {
  key: any,
  label: string,
  data?: any
}
export interface SelectType {
  options: SelectOptionItem[],
  currentValue: SelectOptionItem
}
export interface RequestPageParams {
  pageNum: number,
  pageSize: number
}
//部门更新
export interface DeptParams {
  "searchValue": any,
  "createBy": any,
  "createTime": any,
  "updateBy": any,
  "updateTime": any,
  "remark": any,
  "pageNum": any,
  "pageSize": any,
  "params": any,
  "tenant": any,
  "id": any,
  "parentId": any,
  "ancestors": any,
  "deptName": any,
  "leader": any,
  "leaderUserId": any,
  "leaderUserName": any,
  "grade": any,
  "orderNum": any,
  "status": any,
  "adminGroup": any[]
}

