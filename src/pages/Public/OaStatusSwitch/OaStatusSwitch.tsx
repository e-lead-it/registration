import React, {Component} from 'react';
import {connect} from 'dva';
import {Switch} from "antd";
class OaStatusSwitch extends Component {
  state = {
    status: false
  };
  static defaultProps = {
    currentValue: 0,
    onChange: (res: any) => {
    }
  }
  componentDidMount() {
    this.onChange(this.props.currentValue == 1 ? true : false)
  }
  onChange = (res: any) => {
    let num = res == true ? 1 : 0

    this.setState({
      status: res
    }, (res: any) => {
      this.props.onChange(num)
    })
  }
  render() {
    return <>
      <Switch onChange={this.onChange} checked={this.state.status} checkedChildren="正常" unCheckedChildren="关闭"
              defaultChecked/>
    </>
  }
}
export default connect(
  ({}) => ({}),
)(OaStatusSwitch);
