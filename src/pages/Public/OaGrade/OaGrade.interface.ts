import {SelectOptionItem} from "@/pages/Public/Public.interface";

export interface OaGradeSelectType {
  currentValue: SelectOptionItem,
  options: SelectOptionItem[],
  data?: any
}
