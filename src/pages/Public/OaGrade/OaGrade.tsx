import React, {Component} from 'react';
import {connect} from 'dva';
import {Select} from "antd";
import {OaGradeSelectType} from "./OaGrade.interface";
import {ConnectState} from "@/models/connect";
import {SelectOptionItem} from "@/pages/Public/Public.interface";
import {currentValue} from "@/utils/utils";
import {checkSelectOptionsIsInit} from "@/utils/utils";
import defaultSet from "@/services/defaultSet";
const {Option} = Select;
class OaGrade extends Component {
  state = {
    grade: null,
    selectType: {
      options: defaultSet.DEFAULT_OPTIONS,
      currentValue: defaultSet.DEFAULT_CURRENT_SELECT
    },
  } as {
    selectType: OaGradeSelectType
  };
  static defaultProps = {
    currentValue: null,
    onChange: (res: any) => {
    }
  }
  componentDidMount() {
    checkSelectOptionsIsInit({
      context: this,
      parentGetPropsKey: 'currentValue',
      isFirstEmptyOption: true,
      fetchPath: "global/fetchOrgDeptGrade",
      propsName: "orgDeptGrade",
      selectTypeKeyName: "selectType"
    })
  }
  onChange = (res: any) => {
    currentValue(this, res, 'selectType', (r: any) => {
      this.props.onChange(res.key)
    })
  }
  render() {
    return <Select style={{width: 120}} value={this.state.selectType.currentValue} labelInValue
                   onChange={this.onChange}>
      {this.state.selectType.options.map((res: SelectOptionItem) => {
        return <Option key={res.key.toString()} value={res.key}
                       title={res.label}>{res.label}</Option>
      })}
    </Select>
  }
}
export default connect(
  ({global}: ConnectState) => ({
    orgDeptGrade: global.orgDeptGrade
  }),
)(OaGrade);
