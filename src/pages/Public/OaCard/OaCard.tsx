import React, {Component} from 'react';
import {connect} from 'dva';
import styles from "./OaCard.less";
import {Card} from "antd";
import {get} from "lodash"
import {OaCardProps} from "@/pages/Public/OaCard/OaCard.interface";
class OaCard extends Component {
  state = {};
  static defaultProps: OaCardProps = {
    title: ""
  }
  componentDidMount() {
  }
  render() {
    return <>
      <div className={styles.OaCard}>
        <Card title={get(this.props, "title", false)} bordered={false}>
          {this.props.children}
        </Card>
      </div>
    </>
  }
}
export default connect(
  ({}: {}) => ({}),
)(OaCard);
