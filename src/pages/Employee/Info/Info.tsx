import React, {Component} from 'react';
import {ReactNode} from "react";
import {connect} from 'dva';
import {getToken} from 'chaos-sso-login'
import {ConnectState} from "@/models/connect";
import styles from "./Info.less"
import {Divider} from "antd";
import {Row} from "antd";
import {Col} from "antd";
import {Form} from "antd";
import {Button} from "antd";
import {Input} from "antd";
import {Card} from "antd";
import {Layout} from "antd";
import {DatePicker} from "antd";
import {Select} from "antd";
import {Tabs} from "antd";
import {Upload} from "antd";
import {personal_field_arr} from "./Info.interface";
import {personal_field} from "./Info.interface";
import {getAllFieldInfo} from "./Info.interface";
import {empid} from "./Info.interface";
import {get, isObject} from "lodash"
import {FormInstance} from "antd/lib/form";
import {PlusOutlined} from "@ant-design/icons/lib";
import {MinusCircleOutlined} from "@ant-design/icons/lib";
import {LeftOutlined} from "@ant-design/icons/lib";
import {RightOutlined} from "@ant-design/icons/lib";
import {UploadOutlined} from "@ant-design/icons/lib";
import request from "../../../utils/request";
import moment from "moment";
import {isArray} from "rxjs/internal-compatibility";
import {Descriptions} from "antd";
import {message} from "antd";
import {Result} from "antd";
const {TabPane} = Tabs;
function getError(option, xhr) {
  var msg = 'cannot ' + option.method + ' ' + option.action + ' ' + xhr.status + '\'';
  var err = new Error(msg);
  err.status = xhr.status;
  err.method = option.method;
  err.url = option.action;
  return err;
}
function getBody(xhr) {
  var text = xhr.responseText || xhr.response;
  if (!text) {
    return text;
  }
  try {
    return JSON.parse(text);
  } catch (e) {
    return text;
  }
}
class Info extends Component {
  formRef = React.createRef<FormInstance>();
  step = 0
  addEvent = {}
  isInit = false
  normFile = {}
  state = {
    personalFieldArr: [],
    fieldInfo: {},
    prev: "", next: "", activeKey: "0",
    originData: [],
    uploadList: {},
    second: 6,
    currentState: "edit"//success | edit | scan
  };
  static defaultProps = {}
  cardIdChange = (e: any, item: any, type: personal_field) => {
  }
  componentDidMount() {
    request(OA_HOST_URL + "registration/edit/" + empid, {
      method: "GET"
    }).then((res: any) => {
      if (res.data == true) {
        this.setState({
          currentState: "scan"
        })
      } else {
      }
      let personalFieldArr = personal_field_arr
      let fieldInfo = {}
      let next = ""
      //originData是老数据，会用到id
      getAllFieldInfo().subscribe(({prevRes: result, originData: originData}) => {
        personalFieldArr.forEach((field: any, index: any) => {
          if (index == 1) {
            next = Object.values(field)[0] as string
          }
          fieldInfo[Object.keys(field)[0]] = result[Object.keys(field)[0]]
        })
        //注册事件
        for (let index in fieldInfo) {
          fieldInfo[index].forEach((res: any, i: any) => {
            fieldInfo[index][i]["onChange"] = (e: any, item: any, type: personal_field) => {
              switch (type) {
                //个人信息
                case personal_field.persional:
                  //身份证
                  if (item.key == "cardId") {
                    this.cardIdChange(e, item, type)
                  }
                  break
              }
            }
          })
        }
        this.setState({
          personalFieldArr: personalFieldArr,
          fieldInfo: fieldInfo,
          originData: originData,
          next
        }, (res: any) => {
          for (var i in fieldInfo) {
            //附件材料不在这里设置默认值
            if (i != "materials") {
              let obj = {}
              fieldInfo[i].forEach((item: any, index: any) => {
                obj[get(item, "key", "")] = get(item, "currentValue", "")
              })
              this.formRef.current.setFieldsValue({
                [i]: [
                  obj
                ]
              })
            } else {
              for (var p in fieldInfo['materials']) {
                let key = fieldInfo['materials'][p]['key']
                let uploadList = this.state.uploadList
                uploadList[key] = fieldInfo['materials'][p].data
                uploadList[key]['status'] = "done"
                uploadList[key]['uid'] = fieldInfo['materials'][p].data.id
                this.setState({
                  uploadList
                }, () => {
                  //上传的时候触发
                  this.normFile[key] = e => {
                    let fls = e.fileList
                    if (fls.length > 0) {
                      fls = fls.slice(-1)
                    }
                    if (Array.isArray(e)) {
                      return e;
                    }
                    let file = fls[0]
                    if (file) {
                      let response = get(file, "response", false)
                      //新增
                      if (response && e.fileList.length > 0) {
                        let data = get(response, "data", false)
                        if (data) {
                          let uploadList = this.state.uploadList
                          uploadList[key] = data
                          uploadList[key]['uid'] = data.id
                          uploadList[key]['status'] = "done"
                          this.setState({
                            uploadList
                          })
                        }
                      }
                    }
                    //删除
                    else {
                      let uploadList = this.state.uploadList
                      uploadList[key] = {}
                      this.setState({
                        uploadList
                      })
                    }
                    return e && fls;
                  }
                })
              }
            }
          }
        })
      })
    })
  }
  getSubmitData = (res: any) => {
    request(OA_HOST_URL + "/registration/submit", {
      method: 'POST',
      data: {
        empEducationList: res.education,
        employee: res.employee,
        empPersional: res.persional,
        empBankCardList: res.card,
        empEmergencyContactList: res.contact,
        empFamilyList: res.family,
        empExperienceList: res.experience,
        empCustomerList: res.customer,
        empSkillsList: res.skills,
        empRelationList: res.relation,
        empPersionalMaterials: res.materials,
      },
    }).then((res: any) => {
      if (res.code == 200 && res.data == null) {
        this.setState({
          currentState: "success"
        }, () => {
          setInterval(() => {
            let second = this.state.second
            this.setState({
              second: second - 1
            }, () => {
              if (this.state.second == 0) {
                window.location.reload()
              }
            })
          }, 1000)
        })
      }
    }).catch((error: any) => {
    })
  }
  onFinish = values => {
    for (var i in values) {
      values[i].forEach((item: any, index: any) => {
        item['empId'] = empid
        for (var attr in item) {
          if (isObject(item[attr])) {
            if (moment.isMoment(item[attr])) {
              item[attr] = moment(item[attr]).format("YYYY-MM-DD")
              if (item[attr] == "Invalid date") {
                item[attr] = null
              }
            } else {
              item[attr] = get(item[attr], "key")
            }
          }
        }
      })
    }
    for (var i in values) {
      values[i][0]["id"] = get(get(this.state.originData.filter((item: any, index: any) => {
        return item.key == i
      })[0], "value", {}), "id", "")
    }
    var result = {}
    for (var i in values) {
      //区分数组和对象
      if (i == "persional" || i == "employee") {
        result[i] = values[i][0]
      } else if (i == "materials") {
        let obj = values[i][0]
        for (var x in obj) {
          let target = get(this.state.uploadList, x, false)
          if (target) {
            let id = target.id
            obj[x] = id
          }
        }
        result[i] = obj
      } else {
        result[i] = values[i]
      }
    }
    for (var i in this.state.fieldInfo) {
      let o = this.state.fieldInfo[i]
      o.forEach((item: any, index: any) => {
        if (item.type == "select") {
          if (isArray(result[i])) {
            result[i].forEach((ff: any, ffindex: any) => {
              result[i][ffindex][item.key] = get(item.options.filter((fres: any) => {
                return fres.label == result[i][ffindex][item.key]
              })[0], "key", "")
            })
          } else {
            result[i][item.key] = get(item.options.filter((fres: any) => {
              return fres.label == result[i][item.key]
            })[0], "key", "")
          }
        }
      })
    }
    this.getSubmitData(result)
  };
  onFinishFailed = errorInfo => {
    let errorFields = get(errorInfo, "errorFields", [])
    if (errorFields.length > 0) {
      let errorType = get(errorFields[0], "name")[0]
      let targetIndex = null
      this.state.personalFieldArr.forEach((item: any, index: any) => {
        if (Object.keys(item).indexOf(errorType) == 0) {
          targetIndex = index
        }
      })
      if (targetIndex != null) {
        this.tabChange(targetIndex + "")
        message.error('表单未填写完整');
      }
    }
  };
  setName = (type: any, key: any) => {
    return key
  }
  componentDidUpdate(prevProps: any, prevState: any, snapshot?: any): void {
    if (!this.isInit) {
      for (var i in this.addEvent) {
        this.addEvent[i]()
        this.isInit = true
      }
    }
  }
  uploadOnchange = ({file, fileList}) => {
  }
  beforeUpload = (file, fileList, item) => {
    return true
  }
  customRequest = ({onSuccess, onError, file, myitem}) => {
    return new Promise(((resolve, reject) => {
      request(OA_HOST_URL + "registration/attachment/upload", {
        headers: {"content-type": "multipart/form-data"},
        method: "POST",
        data: {empId: empid, fileType: myitem.fileType, file: file}
      }).then((res) => {
        debugger
        resolve(onSuccess('done'));
      }).catch((error) => {
        debugger
        reject(onError(error));
      })
    }));
  }
  generateFormItem = (field: any, FIELD: any = {}): any => {
    return <>
      <Col xs={24} sm={24} md={24} lg={24} xl={24}
           xxl={24}><Divider>{Object.values(field)[0] as string}</Divider></Col>
      {
        "sub" in field &&
        <Col xs={24} sm={24} md={24} lg={24} xl={24} style={{marginBottom: 12}}
             xxl={24}><i style={{fontSize: 12, fontStyle: "normal"}}>注
            : {get(field, "sub", false)}</i></Col>
      }
      {
        this.state.fieldInfo[Object.keys(field)[0]].map((item: any, itemIndex: any) => {
          let result: ReactNode
          let name = [FIELD.name, this.setName(Object.keys(field)[0], item.key)]
          let fieldKey = [FIELD.fieldKey, this.setName(Object.keys(field)[0], item.key)]
          let label = item.label
          switch (get(item, "type", false)) {
            /*--type select--*/
            case "select": {
              result = <Col xs={24} sm={24} md={12} lg={12} xl={12}
                            style={{paddingRight: "12px"}}>
                <Form.Item
                  {...FIELD}
                  name={name}
                  fieldKey={fieldKey}
                  label={label}
                  rules={[
                    {
                      required: get(item, "required", false),
                      message: '请选择' + label,
                    },
                  ]}
                >
                  <Select>
                    {
                      item.options.map((subitem: any, subindex: any) => {
                        return <Select.Option label={subitem.label} key={subitem.key}
                                              value={subitem.label}>{subitem.label}</Select.Option>
                      })
                    }
                  </Select>
                </Form.Item>
              </Col>
              break
            }
            /*--type day--*/
            case "day": {
              result = <Col xs={24} sm={24} md={12} lg={12} xl={12}
                            style={{paddingRight: "12px"}}>
                <Form.Item
                  {...FIELD}
                  name={name}
                  fieldKey={fieldKey}
                  label={label}
                  rules={[
                    {
                      required: get(item, "required", false),
                      message: '请选择' + label,
                    },
                  ]}
                >
                  <DatePicker style={{width: '100%'}}/>
                </Form.Item>
              </Col>
              break
            }

            /*--type file--*/
            case "file": {
              let token = getToken() ? getToken() : "";
              let flist = "id" in this.state.uploadList[name[1]] ? [this.state.uploadList[name[1]]] : []
              result = <Col xs={24} sm={24} md={12} lg={12} xl={12}
                            style={{paddingRight: "12px"}}>
                <Form.Item
                  {...FIELD}
                  name={name}
                  valuePropName="fileList"
                  getValueFromEvent={this.normFile[item.key]}
                  label={label}
                  initialValue={flist}
                  rules={[
                    {
                      required: get(item, "required", false),
                      message: '请上传' + label,
                    },
                  ]}
                >
                  <Upload
                    /*customRequest={(option) => {
                      var xhr = new XMLHttpRequest();
                      if (option.onProgress && xhr.upload) {
                        xhr.upload.onprogress = function progress(e) {
                          if (e.total > 0) {
                            e.percent = e.loaded / e.total * 100;
                          }
                          option.onProgress(e);
                        };
                      }
                      var formData = new FormData();
                      if (option.data) {
                        Object.keys(option.data).forEach(function (key) {
                          var value = option.data[key];
                          // support key-value array data
                          if (Array.isArray(value)) {
                            value.forEach(function (item) {
                              // { list: [ 11, 22 ] }
                              // formData.append('list[]', 11);
                              formData.append(key + '[]', item);
                            });
                            return;
                          }
                          formData.append(key, option.data[key]);
                        });
                      }
                      if (option.file instanceof Blob) {
                        formData.append(option.filename, option.file, option.file.name);
                      } else {
                        formData.append(option.filename, option.file);
                      }
                      xhr.onerror = function error(e) {
                        option.onError(e);
                      };
                      xhr.onload = function onload() {
                        // allow success when 2xx status
                        // see https://github.com/react-component/upload/issues/34
                        if (xhr.status < 200 || xhr.status >= 300) {
                          return option.onError(getError(option, xhr), getBody(xhr));
                        }
                        option.onSuccess(getBody(xhr), xhr);
                      };
                      xhr.open(option.method, option.action, true);
                      // Has to be after `.open()`. See https://github.com/enyo/dropzone/issues/179
                      if (option.withCredentials && 'withCredentials' in xhr) {
                        xhr.withCredentials = true;
                      }
                      var headers = option.headers || {};
                      // when set headers['X-Requested-With'] = null , can close default XHR header
                      // see https://github.com/react-component/upload/issues/33
                      if (headers['X-Requested-With'] !== null) {
                        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                      }
                      for (var h in headers) {
                        if (headers.hasOwnProperty(h) && headers[h] !== null) {
                          xhr.setRequestHeader(h, headers[h]);
                        }
                      }
                      xhr.send(formData);
                      return {
                        abort: function abort() {
                          xhr.abort();
                        }
                      };
                    }}
                    beforeUpload={file => {
                      console.log(`file------->`, file)
                      /!*const isLt2M = file.size / 1024 / 1024 < 0.5;
                      if (!isLt2M) {
                        message.error(file.name + "图片大小超出限制，请修改后重新上传", 0.8);
                        return false;
                      }*!/
                      return true
                    }}*/
                    multiple={false}
                    headers={{
                      guest: "aaab"
                    }}
                    name={name}
                    action={
                      "http://testoa.e-lead.cn/oa/" + "registration/attachment/upload"
                      //"http://192.168.3.116:8888/oa/" + "registration/attachment/upload"
                    }
                    method={"POST"}
                    data={(file) => {
                      return {empId: empid, fileType: item.fileType, file: file}
                    }} listType="picture">
                    <Button>
                      <UploadOutlined/> 上传
                    </Button>
                  </Upload>
                </Form.Item>
              </Col>
              break
            }
            /*--type input--*/
            case "input": {
              if (item.key == "dateOfBirth") {
                result = <Col xs={24} sm={24} md={12} lg={12} xl={12}
                              style={{paddingRight: "12px"}}>
                  <Form.Item
                    {...FIELD}
                    name={name}
                    fieldKey={fieldKey}
                    label={label}
                    rules={[
                      {
                        required: get(item, "required", false),
                        message: '请填写' + label,
                      },
                    ]}
                  >
                    <Input
                      onChange={(e) => {
                        item.onChange(e, item, Object.keys(field)[0])
                      }} placeholder={`请输入${item.label}`}/>
                  </Form.Item>
                </Col>
              } else {
                result = <Col xs={24} sm={24} md={12} lg={12} xl={12}
                              style={{paddingRight: "12px"}}>
                  <Form.Item
                    {...FIELD}
                    name={name}
                    fieldKey={fieldKey}
                    label={label}
                    rules={[
                      {
                        required: get(item, "required", false),
                        message: '请填写' + label,
                      },
                    ]}
                  >
                    <Input
                      onChange={(e) => {
                        item.onChange(e, item, Object.keys(field)[0])
                      }} placeholder={`请输入${item.label}`}/>
                  </Form.Item>
                </Col>
              }
              break
            }
            default :
              return result = ""
          }
          return result;
        })
      }
    </>
  }
  tabChange = (current: string) => {
    this.step = parseInt(current)
    let prev = ""
    let next = ""
    let personalFieldArr = this.state.personalFieldArr
    if (parseInt(current) == 0) {
      prev = ""
      next = Object.values(personalFieldArr[parseInt(current) + 1])[0]
    } else if (parseInt(current) >= personalFieldArr.length - 1) {
      next = ""
      prev = Object.values(personalFieldArr[parseInt(current) - 1])[0]
    } else {
      prev = Object.values(personalFieldArr[parseInt(current) - 1])[0]
      next = Object.values(personalFieldArr[parseInt(current) + 1])[0]
    }
    this.setState({
      prev, next, activeKey: current
    })
  }
  scan = () => {
    window.location.reload()
  }
  getScanvalue = (res: any) => {
    if (res == null) {
      return <>&nbsp;&nbsp;</>
    } else {
      return <>{res}</>
    }
  }
  render() {
    return <>
      <Layout>
        <div className={styles.Info}>
          {this.state.currentState == "success" &&
          <Col xs={24} sm={24} md={{span: 16, offset: 4}} lg={{span: 16, offset: 4}} xl={{span: 16, offset: 4}}><Card
              title={false} bordered={false}>
              <Result
                  status="success"
                  title="员工信息已录入!"
                  subTitle="祝您工作顺利."
                  extra={[
                    <Button onClick={this.scan} type="primary" key="console">
                      查看信息 ( {this.state.second} s 后跳转 )
                    </Button>,
                  ]}
              />
          </Card></Col>}
          {/*----查看-----*/}
          <Row style={{display: this.state.currentState == "scan" ? "block" : "none"}}>
            <Col xs={24} sm={24} md={{span: 16, offset: 4}} lg={{span: 16, offset: 4}} xl={{span: 16, offset: 4}}>
              {this.state.personalFieldArr.map((field: any, fieldIndex: any) => {
                return <>
                  <Card key={fieldIndex} style={{marginBottom: 12}} title={false} bordered={false}>
                    <Descriptions
                      layout={"vertical"}
                      title={`${Object.values(field)[0]}`}
                      bordered
                      column={{xxl: 3, xl: 2, lg: 1, md: 1, sm: 1, xs: 1}}
                    >
                      {
                        this.state.fieldInfo[Object.keys(field)[0]].map((item: any, itemIndex: any) => {
                          let result: ReactNode
                          let label = item.label
                          switch (get(item, "type", false)) {
                            /*--type select--*/
                            case "select": {
                              result =
                                <Descriptions.Item key={label + "_" + new Date().getTime()}
                                                   label={label}>{this.getScanvalue(get(item, "currentValue", null))}</Descriptions.Item>
                              break
                            }
                            /*--type day--*/
                            case "day": {
                              result = <Descriptions.Item
                                key={label + "_" + new Date().getTime()}
                                label={label}>{
                                get(item, "currentValue", false) ? moment(get(item, "currentValue", false)).format("YYYY年MM月DD日") : " "
                              }</Descriptions.Item>
                              break
                            }

                            /*--type file--*/
                            case "file": {
                              result = <Descriptions.Item key={label + "_" + new Date().getTime()} label={label}>{
                                this.getScanvalue(get(item.data, "url", false) &&
                                    <a href={get(item.data, "url", "")} target={"_blank"}>
                                        <img height={30} src={get(item.data, "url", "")} alt=""/>
                                    </a>)
                              }</Descriptions.Item>
                              break
                            }
                            /*--type input--*/
                            case "input": {
                              result =
                                <Descriptions.Item key={label + "_" + new Date().getTime()}
                                                   label={label}>{this.getScanvalue(get(item, "currentValue", null))}</Descriptions.Item>
                              break
                            }
                            default :
                              return result = null
                          }
                          return result;
                        })
                      }
                    </Descriptions>
                  </Card>
                </>
              })}
            </Col>
          </Row>
          {/*----编辑----*/}
          <Row style={{paddingBottom: 120, display: this.state.currentState == "edit" ? "block" : "none"}}>
            <Col xs={24} sm={24} md={{span: 16, offset: 4}} lg={{span: 16, offset: 4}} xl={{span: 16, offset: 4}}>
              <div style={{
                position: "fixed",
                top: 0,
                left: 0,
                width: "100%",
                background: "#fff",
                zIndex: 4,
                boxShadow: "0 0 13px #ddd"
              }}><Divider><strong>员工信息登记表</strong></Divider></div>
              <Form style={{paddingTop: "60px"}} autoComplete="off" ref={this.formRef} name="control-ref"
                    onFinish={this.onFinish}
                    onFinishFailed={this.onFinishFailed}>
                <Tabs onChange={this.tabChange} tabPosition={"top"} activeKey={this.state.activeKey}>
                  {
                    this.state.personalFieldArr.map((field: any, fieldIndex: any) => {
                      return <TabPane forceRender={true} tab={Object.values(field)[0]} key={fieldIndex}
                                      disabled={fieldIndex === 28}>
                        <Form.List
                          key={fieldIndex}
                          name={Object.keys(field)[0]}>
                          {(FIELDS, {add, remove}) => {
                            return (
                              <>
                                {FIELDS.map((FIELD, INDEX) => {
                                  return <Card key={Object.keys(field)[0]} title={false} bordered={false}
                                               style={{background: "#fff"}}>
                                    <Row key={Object.values(field)[0] as string}>
                                      {
                                        this.generateFormItem(field, FIELD)
                                      }
                                      {
                                        FIELDS.length > 1 &&
                                        <Col xs={24} sm={24} md={24} lg={24} xl={24}
                                             style={{paddingRight: "12px"}}>
                                            <Form.Item>
                                                <Button
                                                    type="dashed"
                                                    onClick={() => {
                                                      remove(FIELD.name);
                                                    }}
                                                    block
                                                >
                                                    <MinusCircleOutlined/> 删除
                                                </Button>
                                            </Form.Item>
                                        </Col>
                                      }
                                      {
                                        get(field, 'canAdd', false) && <Col xs={24} sm={24} md={24} lg={24} xl={24}
                                                                            style={{paddingRight: "12px"}}><Form.Item>
                                            <Button
                                                type="dashed"
                                                onClick={() => {
                                                  add();
                                                }}
                                                block
                                            >
                                                <PlusOutlined/> 添加 {Object.values(field)[0]}
                                            </Button>
                                        </Form.Item>
                                        </Col>
                                      }
                                    </Row>
                                  </Card>
                                })}
                                {this.addEvent[Object.keys(field)[0]] = add}
                              </>
                            );
                          }}
                        </Form.List>
                      </TabPane>
                    })
                  }
                </Tabs>
                <Card className={"submitCard"} title={false} bordered={false} style={{
                  boxShadow: "0 -5px 15px #f5f5f5",
                  background: "none",
                  marginTop: 24,
                  position: "fixed",
                  bottom: 0,
                  left: 0,
                  width: "100%",
                }}>
                  <Row>
                    <Col xs={24} sm={24} md={{span: 16, offset: 4}} lg={{span: 16, offset: 4}}
                         xl={{span: 16, offset: 4}} style={{background: "#fff"}}>
                      <Form.Item>
                        {
                          this.state.next != "" && this.state.prev == "" &&
                          <Row>
                              <Col span={24}>
                                  <Button onClick={() => {
                                    this.tabChange((this.step + 1).toString())
                                  }} className={styles.mbtn} block={true} type="primary" style={{marginTop: 24}}>
                                    {this.state.next}
                                      <RightOutlined/>
                                  </Button>
                              </Col>
                          </Row>
                        }
                        {
                          this.state.next == "" && this.state.prev != "" && this.step != this.state.personalFieldArr.length - 1 &&
                          <Row>
                              <Col span={24}>
                                  <Button onClick={() => {
                                    this.tabChange((this.step - 1).toString())
                                  }} className={styles.mbtn} block={true} type="default" style={{marginTop: 24}}>
                                      <LeftOutlined/>
                                      <div className={styles.text}>{this.state.prev}</div>
                                  </Button>
                              </Col>
                          </Row>
                        }
                        {
                          this.state.next != "" && this.state.prev != "" && <Row>
                              <Col span={10} offset={2}>
                                  <Button onClick={() => {
                                    this.tabChange((this.step - 1).toString())
                                  }} className={styles.mbtn} block={true} type="default" style={{marginTop: 24}}>
                                      <LeftOutlined/>
                                      <div className={styles.text}>{this.state.prev}</div>
                                  </Button>
                              </Col>
                              <Col span={1}></Col>
                              <Col span={9}>
                                  <Button onClick={() => {
                                    this.tabChange((this.step + 1).toString())
                                  }} className={styles.mbtn} block={true} type="primary" style={{marginTop: 24}}>
                                      <div className={styles.text}>{this.state.next}</div>
                                      <RightOutlined/>
                                  </Button>
                              </Col>
                              <Col span={2}></Col>
                          </Row>
                        }
                        {
                          this.state.next == "" && this.state.prev != "" && this.step == this.state.personalFieldArr.length - 1 &&
                          <Row>
                              <Col span={10} offset={2}>
                                  <Button onClick={() => {
                                    this.tabChange((this.step - 1).toString())
                                  }} className={styles.mbtn} block={true} type="default" style={{marginTop: 24}}>
                                      <LeftOutlined/>
                                      <div className={styles.text}>({this.state.prev})</div>
                                  </Button>
                              </Col>
                              <Col span={1}></Col>
                              <Col span={9}>
                                  <Button block={true} htmlType="submit" type="primary" style={{marginTop: 24}}>
                                      提交
                                  </Button>
                              </Col>
                              <Col span={2}></Col>
                          </Row>
                        }
                      </Form.Item>
                    </Col>
                  </Row>
                </Card>
              </Form>
            </Col>
          </Row>
        </div>
      </Layout>
    </>
  }
}
export default connect(
  ({global}: ConnectState) => ({}),
)(Info);
