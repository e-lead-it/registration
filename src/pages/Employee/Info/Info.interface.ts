import {Observable} from "rxjs"
import {mergeMap} from "rxjs/operators"
import request from "../../../utils/request";
import {get, isArray, cloneDeep} from "lodash"
import moment from "moment";
var url = new URL(window.location.href)
let empId = url.searchParams.get("empId")
export const empid = empId
export enum personal_field {
  employee = "employee",
  persional = "persional",
  education = "education",
  card = "card",
  contact = "contact",
  family = "family",
  experience = "experience",
  customer = "customer",
  skills = "skills",
  relation = "relation",
  materials = "materials",
}
export const personal_field_arr = [
  {employee: "基本信息"},///oa/registration/employee
  {persional: "个人信息"},///oa/registration/persional
  {education: "教育背景", sub: "由最高学历起依次填写", canAdd: true},///oa/registration/education/batch //批量
  {card: "银行卡信息", canAdd: true},///oa/registration/card
  {contact: "紧急联系人", canAdd: true},///oa/registration/contact/batch //批量
  {family: "家庭及主要社会关系", canAdd: true},///oa/registration/family/batch //批量
  {experience: "工作经历", sub: "由近及远依次填写", canAdd: true},///oa/registration/experience/batch //批量
  {customer: "相关客户项目经历", sub: "如有则必填，如无则填写“无”", canAdd: true},///oa/registration/customer/batch //批量
  {skills: "技能等级证书", sub: "如有则请勾选对应方框，如无则请略过", canAdd: true},///oa/registration/skills/batch //批量
  {
    relation: "关联关系申报",
    sub: "如有亲属现就职于员工本人入职部门的业务领域所对应的客户公司则必填，如无则填写“无”） （亲属含：①配偶和子女；②夫妻双方的父母、亲/堂/表兄弟姐妹；③上述两大类人员的配偶",
    canAdd: true
  },///oa/registration/relation/batch //批量
  {materials: "个人材料"},
]
export const getAllFieldInfo = (): Observable<any> => {
  return new Observable(ob => {
    //初始化下拉
    Promise.all([
      request(OA_HOST_URL + "registration/dict/data/dictType/emp_edu_enrollment"),//是否全日制unifiedEnrollment
      request(OA_HOST_URL + "registration/dict/data/dictType/emp_edu_degree"),//学位 硕士。。。degree
      request(OA_HOST_URL + "registration/dict/data/dictType/emp_edu_education"),//学历 本科。。。education
      request(OA_HOST_URL + "registration/dict/data/dictType/emp_politics_status"),//政治面貌
      request(OA_HOST_URL + "registration/dict/data/dictType/sys_user_sex"),//性别
      request(OA_HOST_URL + "registration/dict/data/dictType/emp_marital_status"),//婚姻
      request(OA_HOST_URL + "registration/dict/data/dictType/emp_house_hole"),//户口类型
      request(OA_HOST_URL + "registration/dict/data/dictType/emp_skills_type"),//证书类型
      request(OA_HOST_URL + "registration/dict/data/dictType/emp_customer_type"),//相关客户经历
    ]).then((res: any) => {
      let unifiedEnrollment = res[0].data.map((item: any, index: any) => {
        return {
          key: item.dictValue,
          label: item.dictLabel
        }
      })
      let degree = res[1].data.map((item: any, index: any) => {
        return {
          key: item.dictValue,
          label: item.dictLabel
        }
      })
      let education = res[2].data.map((item: any, index: any) => {
        return {
          key: item.dictValue,
          label: item.dictLabel
        }
      })
      let politicsStatus = res[3].data.map((item: any, index: any) => {
        return {
          key: item.dictValue,
          label: item.dictLabel
        }
      })
      let sex = res[4].data.map((item: any, index: any) => {
        return {
          key: item.dictValue,
          label: item.dictLabel
        }
      })
      let maritalStatus = res[5].data.map((item: any, index: any) => {
        return {
          key: item.dictValue,
          label: item.dictLabel
        }
      })
      let householeType = res[6].data.map((item: any, index: any) => {
        return {
          key: item.dictValue,
          label: item.dictLabel
        }
      })
      let certificate = res[7].data.map((item: any, index: any) => {
        return {
          key: item.dictValue,
          label: item.dictLabel
        }
      })
      let customer = res[8].data.map((item: any, index: any) => {
        return {
          key: item.dictValue,
          label: item.dictLabel
        }
      })
      ob.next({
        unifiedEnrollment,
        degree,
        education,
        politicsStatus,
        sex,
        maritalStatus,
        householeType,
        certificate,
        customer
      })
      ob.complete()
    })
  }).pipe(
    mergeMap((res: any) => {
      return new Observable(ob => {
        var obj = {}
        for (var i in personal_field) {
          obj[i] = []
        }
        for (var i in obj) {
          if (personal_field.employee == i) {
            obj[i] = [
              {key: "userName", label: "姓名", required: false, type: "input"},
              /*{key: "loginName", label: "登录名"},*/
              /*{key: "deptName", label: "部门"},*/
              {key: "phone", label: "手机", required: false, type: "input"},
              {key: "email", label: "邮箱", required: false, type: "input"},
              /*{key: "eleadEmail", label: "企业邮箱", required: false, type: "input"}*/
            ]
          } else if (personal_field.persional == i) {
            obj[i] = [{key: "cardId", label: "证件号码", type: "input", required: true},
              {key: "cardName", label: "身份证姓名", required: true, type: "input"},
              {key: "cardAddress", label: "身份证地址", required: true, type: "input"},
              /*{key: "cardVaildity", label: "证件有效期"},*/
              {key: "dateOfBirth", label: "出生日期", required: true, type: "day"},
              {key: "age", label: "年龄", required: true, type: "number"},
              {
                key: "sex",
                label: "性别",
                required: false,
              },
              {key: "nation", label: "民族", required: false},
              {
                key: "maritalStatus", label: "婚姻状况", required: false,
                type: "select",
                options: res.maritalStatus,
              },
              {
                key: "householeType", label: "户籍类型", required: false, type: "select",
                options: res.householeType,
              },
              {key: "address", label: "住址", required: false},
              {
                key: "politicsStatus", label: "政治面貌", required: false,
                type: "select",
                options: res.politicsStatus,
              }]
          } else if (personal_field.education == i) {
            obj[i] = [
              {key: "admissionTime", label: "起始日期", required: true, type: "day"},
              {key: "graduateTime", label: "结束日期", required: true, type: "day"},
              {key: "graduateInstitution", label: "学校名称(全称)", required: true, type: "input"},
              {key: "professional", label: "专业名称(全称)", required: true, type: "input"},
              {
                key: "unifiedEnrollment",
                label: "教育性质",
                required: false,
                type: "select",
                options: res.unifiedEnrollment,
              },
              {
                key: "education",
                label: "学历",
                required: false,
                type: "select",
                options: res.education,
              },
              {
                key: "degree",
                label: "学位",
                required: false,
                type: "select",
                options: res.degree,
              },
            ]
          } else if (personal_field.card == i) {
            obj[i] = [
              {key: "bankCard", label: "银行卡号", required: false, type: "input"},
              {key: "bankOfDeposit", label: "开户行", required: false, type: "input"},
            ]
          } else if (personal_field.contact == i) {
            obj[i] = [
              {key: "contacName", label: "联系人姓名", required: false, type: "input"},
              {key: "contacRelationship", label: "与本人关系", required: false, type: "input"},
              {key: "contacNumber", label: "联系人电话", required: false, type: "input"},
            ]
          } else if (personal_field.family == i) {
            obj[i] = [
              {key: "familyName", label: "姓名", required: false, type: "input"},
              {key: "familyRelationship", label: "直系亲属", required: false, type: "input"},
              {key: "familySex", label: "性别", required: false, type: "select", options: res.sex,},
              /*{key: "familyBirthday", label: "生日"},*/
              {key: "familyNumber", label: "电话", required: false, type: "input"},
              /*{key: "unit", label: "工作单位"},*/
              {key: "positionsHeld", label: "担任职务", required: false, type: "input"},
              {key: "location", label: "所在地", required: false, type: "input"},
            ]
          } else if (personal_field.experience == i) {
            obj[i] = [
              {key: "entryTime", label: "起始日期", required: false, type: "day"},
              {key: "leaveTime", label: "结束日期", required: false, type: "day"},
              {key: "corporate", label: "工作单位", required: false, type: "input"},
              {key: "dept", label: "离职前部门", required: false, type: "input"},
              {key: "post", label: "离职前职务", required: false, type: "input"},
              {key: "level", label: "离职前职级", required: false, type: "input"},
              {key: "jobNumber", label: "工号", required: false, type: "input"},
              {key: "reterence", label: "证明人", required: false, type: "input"},
              {key: "phone", label: "证明人电话", required: false, type: "input"},
            ]
          } else if (personal_field.customer == i) {
            obj[i] = [
              {key: "entryTime", label: "首次入项时间", required: false, type: "day"},
              {key: "customer", label: "项目所属客户", required: false, type: "select", options: res.customer},
              {key: "leaveYears", label: "离开项目年限", required: false, type: "input"},
            ]
          } else if (personal_field.materials == i) {
            obj[i] = [
              {key: "cardFront", label: "身份证（正）", required: false, type: "file", fileType: 1},
              {key: "cardOppositeSide", label: "身份证（反）", required: false, type: "file", fileType: 1},
              {key: "eduaction", label: "学历证书", required: false, type: "file", fileType: 1},
              {key: "diploma", label: "学位证书", required: false, type: "file", fileType: 1},
              {key: "resignationCertificate", label: "离职证明", required: false, type: "file", fileType: 1},
              {key: "photo", label: "员工照片", required: false, type: "file", fileType: 1},
              {key: "healthReport", label: "健康报告", required: false, type: "file", fileType: 1},
              {key: "resume", label: "个人简历", required: false, type: "file", fileType: 2},
              {key: "chsiPrintscreen", label: "学信网截图", required: false, type: "file", fileType: 1},
              {key: "studentCertificate", label: "学籍报告", required: false, type: "file", fileType: 1},
            ]
          } else if (personal_field.relation == i) {
            obj[i] = [
              {key: "familyName", label: "亲属姓名", required: false, type: "input"},
              {key: "company", label: "亲属现任职于", required: false, type: "select", options: res.customer},
              {key: "familyRelationship", label: "与本人关系", required: false, type: "input"},
              {key: "deptName", label: "亲属所在部门", required: false, type: "input"},
              {key: "level", label: "亲属目前职级", required: false, type: "input"},
              {key: "no", label: "亲属身份证号码或在客户公司工号", required: false, type: "input"},
            ]
          } else if (personal_field.skills == i) {
            obj[i] = [
              {key: "certificate", label: "证书类型", required: false, type: "select", options: res.certificate},
              {key: "grade", label: "证书等级", required: false, type: "input"},
            ]
          }
        }
        ob.next(obj)
        ob.complete()
      })
    }),
    mergeMap((prevRes: any) => {
      return new Observable(ob => {
        var arr = [
          {key: "employee", value: request(OA_HOST_URL + "/registration/employee/" + empid)},//基本信息
          {key: "persional", value: request(OA_HOST_URL + "/registration/persional/details/" + empid)},//个人信息
          {key: "education", value: request(OA_HOST_URL + "/registration/education/details/" + empid)},//教育背景
          {key: "card", value: request(OA_HOST_URL + "/registration/card/details/" + empid)},//银行卡信息
          {key: "contact", value: request(OA_HOST_URL + "/registration/contact/details/" + empid)},//紧急联系人
          {key: "family", value: request(OA_HOST_URL + "/registration/family/details/" + empid)},//家庭及主要社会关系
          {key: "experience", value: request(OA_HOST_URL + "/registration/experience/details/" + empid)},//工作经历
          {key: "customer", value: request(OA_HOST_URL + "/registration/customer/details/" + empid)},//相关客户项目经历
          {key: "skills", value: request(OA_HOST_URL + "/registration/skills/details/" + empid)},//技能等级证书
          {key: "relation", value: request(OA_HOST_URL + "/registration/relation/details/" + empid)},//关联关系
          {key: "materials", value: request(OA_HOST_URL + "/registration/materials/details/" + empid)},//个人材料
        ]
        Promise.all(arr.map((res: any) => {
          return res.value
        })).then((arrRes: any) => {
          let result = arr.map((item, index) => {
            return {
              key: item.key,
              value: arrRes[index].data
            }
          })
          result.forEach((item1: any, index1: any) => {
            prevRes[item1.key].forEach((item2: any, index2: any) => {
              //如果是数组，取第一个
              if (isArray(item1.value)) {
                item1.value = item1.value[0]
              }
              for (let i in item1.value) {
                if (item2.key == i) {
                  switch (item2.type) {
                    case "day": {
                      item2.currentValue = item1.value[i] ? moment(item1.value[i]) : null
                      break
                    }
                    case "select": {
                      if (item1.value[i] == null) {
                        item2.currentValue = item2.options[0].label
                      } else {
                        //item2.currentValue = {key: item1.value[i], label: item1.value[i]}
                        item2.currentValue = get(item2.options.filter((fitem: any, findex: any) => {
                          return fitem.key == item1.value[i]
                        })[0], "label", "")
                      }
                      break
                    }
                    default:
                      item2.currentValue = item1.value[i]
                  }
                }
              }
            })
          })
          ob.next({prevRes, originData: result})
          ob.complete()
        })
      })
    }),
    //处理附件
    mergeMap(({prevRes, originData: result}: any) => {
      return new Observable(ob => {
        let materials = prevRes.materials
        var arr = []
        materials.forEach((item: any, index: any) => {
          if (item.currentValue != null) {
            arr.push(request(OA_HOST_URL + "registration/attachment/" + item.currentValue))
          }
        })
        Promise.all(arr).then((res: any) => {
          let tindex = 0
          prevRes.materials = prevRes.materials.map((item: any, index: any) => {
            if (item.currentValue != null) {

              item.data = get(res[tindex], "data", {})
              tindex++
            } else {
              item.data = {}
            }
            return item
          })
          ob.next({prevRes, originData: result})
          ob.complete()
        })
      })
    })
  )
}

