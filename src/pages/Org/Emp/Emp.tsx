import React, {Component} from 'react';
import {connect} from 'dva';
import {Card} from "antd";
import {PageHeader} from "antd";
import {Row} from "antd";
import {Col, Comment} from "antd";
import {Divider} from "antd";
import {Menu} from "antd";
import styles from "./Emp.less";
import {get} from "lodash"
import OaUser from "@/pages/Public/OaUser/OaUser";
import {ConnectState} from "@/models/connect";
import {Avatar} from "antd";
import {UserOutlined} from "@ant-design/icons/lib";
class Emp extends Component {
  state = {
    employee: []
  };
  componentDidMount() {
    const {employee, dispatch} = this.props
    if (employee) {
      this.getEmployee()
    } else {
      if (dispatch) {
        dispatch({
          type: "global/fetchEmployee",
          payload: {
            pageNum: 1,
            pageSize: 100,
            status: 1
          }
        }).then((res: any) => {
          this.getEmployee()
        })
      }
    }
  }
  getEmployee = () => {
    let rows = get(this.props.employee, "rows", [])
    this.setState({
      employee: rows
    })
  }
  userSearchChange = (res: any) => {
  }
  OaUserOnChange = (res: any) => {
  }
  render() {
    return <>
      <div className={styles.Emp}>
        <PageHeader style={{background: "#fff", marginBottom: 12}} title={
          <>
          </>
        }></PageHeader>
        <Row gutter={12}>
          <Col span={6}>
            <Card className={'oaCard'} bordered={false} title={false}>
              <OaUser currentValue={""} onChange={this.OaUserOnChange}/>
              <Menu
                style={{width: "100%", marginTop: 8, maxHeight: 800, overflowY: "auto"}}
                defaultSelectedKeys={['1']}
                defaultOpenKeys={['sub1']}
                mode="inline"
              >
                {
                  this.state.employee.map((res: any) => {
                    return <Menu.Item key={res.id}>
                      <Comment
                        author={<a> {res.deptName}</a>}
                        avatar={
                          <Avatar style={{backgroundColor: '#87d068'}} icon={<UserOutlined/>}/>
                        }
                        content={
                          <p>
                            {res.userName}
                          </p>
                        }
                        datetime={res.userId}
                      />
                    </Menu.Item>
                  })
                }
              </Menu>
              {/*<List
                style={{marginTop: 8, maxHeight: 800, overflowY: "auto"}}
                itemLayout="horizontal"
                dataSource={this.state.employee}
                renderItem={item => (
                  <List.Item>
                    <List.Item.Meta
                      avatar={<Avatar style={{backgroundColor: '#87d068'}} icon={<UserOutlined/>}/>}
                      title={item.userName}
                      description={
                        <>
                          {item.userId} <Divider type={"vertical"}/> {item.deptName}
                        </>
                      }
                    />
                  </List.Item>
                )}
              />*/}
            </Card>
          </Col>
          <Col span={18}>
            <Card className={'oaCard'} bordered={false} title={false}></Card>
          </Col>
        </Row>
      </div>
    </>
  }
}
export default connect(
  ({global}: ConnectState) => ({
    employee: global.employee
  }),
)(Emp);
