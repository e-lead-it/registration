// https://umijs.org/config/
import {defineConfig} from 'umi';
import defaultSettings from './defaultSettings';
import proxy from './proxy';

const {REACT_APP_ENV} = process.env;
export default defineConfig({
  hash: true,
  antd: {},
  dva: {
    hmr: true,
  },
  locale: {
    // default zh-CN
    default: 'zh-CN',
    // default true, when it is true, will use `navigator.language` overwrite default
    antd: true,
    baseNavigator: true,
  },
  dynamicImport: {
    loading: '@/components/PageLoading/index',
  },
  targets: {
    ie: 10,
  },
  // umi routes: https://umijs.org/docs/routing
  routes: [
    {
      path: "/entryForm",
      component: '../layouts/BlankLayout',
      authority: ['admin'],
      routes: [
        {
          name: 'info',
          icon: 'table',
          path: '/entryForm',
          component: './Employee/Info/Info',
          hideInMenu: true
        }
      ]
    },
    {
      path: '/',
      component: '../layouts/SecurityLayout',
      routes: [
        /*{
          path: "/entryForm",
          component: '../layouts/BlankLayout',
          authority: ['admin'],
          routes: [
            {
              name: 'info',
              icon: 'table',
              path: '/entryForm',
              component: './Employee/Info/Info',
              hideInMenu: true
            }
          ]
        },*/
        {
          path: '/other',
          component: '../layouts/BasicLayout',
          authority: ['admin'],
          routes: [
            {
              path: '/',
              redirect: '/Org/Dept',
            },
            {
              name: 'list.table-list',
              icon: 'table',
              path: '/list',
              component: './ListTableList',
              hideInMenu: true
            },
            //组织结构
            {
              path: '/Org',
              name: 'Org',
              icon: 'ApartmentOutlined',
              routes: [
                //部门管理
                {
                  name: 'Dept',
                  icon: 'SlidersOutlined',
                  path: '/Org/Dept',
                  component: './Org/Dept/Dept',
                },
                //人员管理
                {
                  name: 'Emp',
                  icon: 'SlidersOutlined',
                  path: '/Org/Emp',
                  component: './Org/Emp/Emp',
                },
                //职位管理
                {
                  name: 'Position',
                  icon: 'SlidersOutlined',
                  path: '/Org/Position',
                  component: './Org/Position/Position',
                },
              ]
            },
            {
              component: './404',
            },
          ],
        },
        {
          component: './404',
        },
      ],
    },
    {
      component: './404',
    },
  ],
  // Theme for antd: https://ant.design/docs/react/customize-theme-cn
  theme: {
    // ...darkTheme,
    'primary-color': defaultSettings.primaryColor,
  },
  // @ts-ignore
  title: false,
  ignoreMomentLocale: true,
  proxy: proxy[REACT_APP_ENV || 'dev'],
  publicPath: '/',
  manifest: {
    basePath: '/',
  },
  chainWebpack: (config, {webpack}) => {

    /*    config.merge({
          optimization: {
            minimize: true,
            splitChunks: {
              chunks: 'all',
              minSize: 30000,
              minChunks: 2,
              automaticNameDelimiter: '.',
              cacheGroups: {
                vendors: {
                  chunks: 'all',
                  test: /[\\/]node_modules[\\/]/,
                  priority: 0,
                  minChunks: 2,
                  name: 'vendors',
                  reuseExistingChunk: true
                },
                antdesigns: {
                  name: 'antdesigns',
                  chunks: 'all',
                  test: /[\\/]node_modules[\\/](@ant-design|antd)[\\/]/,
                  priority: 1,
                },
                default: {
                  minChunks: 2,
                  priority: -11,
                  reuseExistingChunk: true
                }
              }
            }
          }
        })*/
    ;
    /*switch (REACT_APP_ENV) {
      case "uat": {
      }
        break
      case "dev": {
      }
    }*/
  },
  define: (() => {
    let obj = {}
    switch (REACT_APP_ENV) {
      case "uat": {
        obj = {
          //# 开发环境配置
          ENV: 'uat',
          //# port = '8888'
          //# 易立德OA / 开发环境
          BASE_API: '/uat-api',
          //# 前端地址
          PORTAL_HOST_URL: 'http://testoa2.e-lead.cn/',
          //# 易立德OA
          OA_HOST_URL: 'http://testoa.e-lead.cn/oa/',
          //# 上传地址
          UPLOAD_URL: 'http://testoa.e-lead.cn/oa/',
          //# 单点服务后台
          SSO_HOST_URL: 'http://sso.e-lead.cn/sso/',
          //# SSO
          //# corpId
          SSO_CORP_ID: 'ding72d45292a8b0a17024f2f5cc6abecb85',
          //# 浏览器存储的键
          SSO_SESSION_TOKEN_KEY: 'sso-token',
          SSO_SESSION_USER_KEY: 'sso-user',
          //# 认证TOEKN
          SSO_HEADER_TOKEN: 'Authorization',
          //# 认证方式
          SSO_RESPONSE_TYPE: 'code',
          //# 认证客户端ID
          SSO_CLIENT_ID: 'elead_oa',
          //# 单点退出地址
          //#SSO_LOGOUT_URL = 'code'
          //# 登录成功回调地址
          SSO_CALLBACK_URL: 'codeCallback',
        }
        break
      }
      case "dev": {
        obj = {
          //# 开发环境配置
          ENV: 'development',
          //# port = '8888'
          //# 易立德OA / 开发环境
          BASE_API: '/dev-api',
          //# 前端地址
          PORTAL_HOST_URL: 'http://localhost:8000/',
          //# 易立德OA
          OA_HOST_URL: 'http://testoa.e-lead.cn/oa/',
          //# 上传地址
          UPLOAD_URL: 'http://testoa.e-lead.cn/oa/',
          //# 单点服务后台
          SSO_HOST_URL: 'http://sso.e-lead.cn/sso/',
          //# SSO
          //# corpId
          SSO_CORP_ID: 'ding72d45292a8b0a17024f2f5cc6abecb85',
          //# 浏览器存储的键
          SSO_SESSION_TOKEN_KEY: 'sso-token',
          SSO_SESSION_USER_KEY: 'sso-user',
          //# 认证TOEKN
          SSO_HEADER_TOKEN: 'Authorization',
          //# 认证方式
          SSO_RESPONSE_TYPE: 'code',
          //# 认证客户端ID
          SSO_CLIENT_ID: 'elead_oa',
          //# 单点退出地址
          //#SSO_LOGOUT_URL = 'code'
          //# 登录成功回调地址
          SSO_CALLBACK_URL: 'codeCallback',
        }
        break
      }
    }
    return obj
  })()
});
